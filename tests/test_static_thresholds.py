import unittest
from check_graphite import NagiosReturnCode, process_static_threshold


metrics_dp = {'target': 'metricsReceived',
              'datapoints': [[None,   1642497960],
              [None,   1642498020],
              [1403.0, 1642498080],
              [1387.0, 1642498140],
              [615.0,  1642498200],
              [618.0,  1642498260],
              [615.0,  1642498320],
              [621.0,  1642498380],
              [None,   1642498440],
              [None,   1642498500]
              ]}
percentile_dp = {'target': 'nPercentile(metricsReceived, 70)',
                 'datapoints' : [[1387.0, 1642497960],
                 [1387.0, 1642498020],
                 [1387.0, 1642498080],
                 [1387.0, 1642498140],
                 [1387.0, 1642498200],
                 [1387.0, 1642498260],
                 [1387.0, 1642498320],
                 [1387.0, 1642498380],
                 [1387.0, 1642498440],
                 [1387.0, 1642498500]
                 ]}


class TestStaticThreshold(unittest.TestCase):
    def test_no_thresholds(self):
        self.assertEqual(process_static_threshold(metrics_dp).return_code, NagiosReturnCode.OK)

    def test_count(self):
        self.assertEqual(process_static_threshold(metrics_dp, count=3, warning=2000).return_code, NagiosReturnCode.OK)
        self.assertEqual(process_static_threshold(metrics_dp, count=7, warning=2000).return_code, NagiosReturnCode.UNKNOWN)
        self.assertEqual(process_static_threshold(metrics_dp, count=7, warning=2000, return_code_when_empty=NagiosReturnCode.OK).return_code, NagiosReturnCode.OK)

    def test_critical(self):
        self.assertEqual(process_static_threshold(metrics_dp, critical=2000).return_code, NagiosReturnCode.OK)
        self.assertEqual(process_static_threshold(metrics_dp, critical=1200).return_code, NagiosReturnCode.CRITICAL)
        self.assertEqual(process_static_threshold(metrics_dp, critical=1200, count=3).return_code, NagiosReturnCode.OK)

    def test_warning(self):
        self.assertEqual(process_static_threshold(metrics_dp, warning=2000).return_code, NagiosReturnCode.OK)
        self.assertEqual(process_static_threshold(metrics_dp, warning=1200).return_code, NagiosReturnCode.WARNING)
        self.assertEqual(process_static_threshold(metrics_dp, warning=1200, count=3).return_code, NagiosReturnCode.OK)
        self.assertEqual(process_static_threshold(metrics_dp, warning=1000, critical=1400, count=2).return_code, NagiosReturnCode.WARNING)

    def test_percentile(self):
        self.assertEqual(process_static_threshold(percentile_dp, percentile=70, warning=2000).return_code, NagiosReturnCode.OK)
        self.assertEqual(process_static_threshold(percentile_dp, percentile=70, warning=1200).return_code, NagiosReturnCode.WARNING)
        self.assertEqual(process_static_threshold(percentile_dp, percentile=70, warning=1200, critical=1400).return_code, NagiosReturnCode.WARNING)
        self.assertEqual(process_static_threshold(percentile_dp, percentile=70, warning=1200, critical=1300).return_code, NagiosReturnCode.CRITICAL)

    def test_over(self):
        self.assertEqual(process_static_threshold(metrics_dp, count=3, warning=2000, over=True).return_code, NagiosReturnCode.OK)
        self.assertEqual(process_static_threshold(metrics_dp, count=3, warning=616, over=True).return_code, NagiosReturnCode.WARNING)

    def test_under(self):
        self.assertEqual(process_static_threshold(metrics_dp, count=3, warning=2000, over=False).return_code, NagiosReturnCode.WARNING)
        self.assertEqual(process_static_threshold(metrics_dp, count=3, warning=616, over=False).return_code, NagiosReturnCode.OK)
