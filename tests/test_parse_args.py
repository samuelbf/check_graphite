import unittest
import unittest.mock
from check_graphite import parse_args


class TestParseArgs(unittest.TestCase):
    def test_from(self):
        try:
            parse_args(['--from', '-10minutes', '-t', 'metricsReceived'])
        except SystemExit:
            self.fail('Failed to parse from argument')

    def test_missing_args(self):
        with self.assertRaises(SystemExit):
            parse_args(['--from', '-10minutes'])

        with self.assertRaises(SystemExit):
            parse_args(['-t', 'metricsReceived'])
