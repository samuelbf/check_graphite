import unittest
import unittest.mock
from io import StringIO
from test_fetchmetric import fake_graphite_server
import check_graphite
from check_graphite import main, NagiosReturnCode


class TestEndToEnd(unittest.TestCase):
    common_argv = ['check_graphite.py', '-U', 'http://localhost:8888', '--from=-10minutes']

    def assert_return_code(self, argv, expected_return_code):
        check_graphite.urllib.request.urlopen = fake_graphite_server
        with self.assertRaises(SystemExit) as catched:
            main(self.common_argv + argv)

        self.assertEqual(catched.exception.code, expected_return_code.value)

    def test_invalid_series(self):
        local_argv = ['-t', 'nonExistingMetric']
        self.assert_return_code(local_argv, NagiosReturnCode.UNKNOWN)
        self.assert_return_code(local_argv + ['--empty-ok'], NagiosReturnCode.OK)

    def test_single_series(self):
        local_argv = ['-t', 'metricsReceived']
        self.assert_return_code(local_argv + ['-W', '3000'], NagiosReturnCode.OK)
        self.assert_return_code(local_argv + ['-W', '2000'], NagiosReturnCode.OK)
        self.assert_return_code(local_argv + ['-W', '1200'], NagiosReturnCode.WARNING)
        self.assert_return_code(local_argv + ['-W', '1200', '--count', '3'], NagiosReturnCode.OK)
        self.assert_return_code(local_argv + ['-W', '1200', '-C', '1500'], NagiosReturnCode.WARNING)
        self.assert_return_code(local_argv + ['-W', '1000', '-C', '1200'], NagiosReturnCode.CRITICAL)

    def test_long_output(self):
        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_out:
            self.assert_return_code(['-t', 'metricsReceived', '--from=-1day'], NagiosReturnCode.OK)
            stdout = fake_out.getvalue()

        self.assertEqual(stdout, """GRAPHITE OK : metricsReceived is 14131.0 (highest value) | metricsReceived=14131.0;;;;
metricsReceived=None/None/1403.0/1387.0/615.0/618.0/615.0/621.0/None/None/None/None/None/849.0/1639.0/.../320.0/319.0/319.0/317.0/317.0/320.0/317.0/317.0/318.0/317.0
""")

    def test_several_series(self):
        local_argv = ['-t', 'metricsReceived', '-t', 'committedPoints']
        self.assert_return_code(local_argv + ['-W', '3000'], NagiosReturnCode.OK)
        self.assert_return_code(local_argv + ['-W', '2000'], NagiosReturnCode.WARNING)
        self.assert_return_code(local_argv + ['-W', '1200'], NagiosReturnCode.WARNING)
        self.assert_return_code(local_argv + ['-W', '1200', '--count', '3'], NagiosReturnCode.OK)
        self.assert_return_code(local_argv + ['-W', '1200', '-C', '3000'], NagiosReturnCode.WARNING)
        self.assert_return_code(local_argv + ['-W', '1000', '-C', '1200'], NagiosReturnCode.CRITICAL)

    def test_percentile_series(self):
        local_argv = ['-t', 'metricsReceived', '--percentile', '70']
        self.assert_return_code(local_argv + ['-W', '3000'], NagiosReturnCode.OK)
        self.assert_return_code(local_argv + ['-W', '1200'], NagiosReturnCode.WARNING)
        self.assert_return_code(local_argv + ['-W', '1000', '-C', '1200'], NagiosReturnCode.CRITICAL)

    def test_graphite_expand(self):
        # Comparing '-t metricsReceived -t committedPoints' with '-t "{metricsReceived,committedPoints}"' (we expect the same output) :
        stdout_1t = ''
        stdout_2t = ''
        check_graphite.urllib.request.urlopen = fake_graphite_server
        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_out1:
            try:
                main(self.common_argv + ['-t', '{metricsReceived,committedPoints}', '-W', '2000'])
            except SystemExit:
                pass
            stdout_1t = fake_out1.getvalue()
        with unittest.mock.patch('sys.stdout', new=StringIO()) as fake_out2:
            try:
                main(self.common_argv + ['-t', 'metricsReceived', '-t', 'committedPoints', '-W', '2000'])
            except SystemExit:
                pass
            stdout_2t = fake_out2.getvalue()

        self.assertEqual(stdout_2t, stdout_1t)
