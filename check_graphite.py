#!/usr/bin/env python3
"""
check_graphite.py
~~~~~~~

:copyright: (c) 2012 DISQUS, (c) 2022 Samuel BF
:license: Apache License 2.0, see LICENSE for more details.
:url: https://gitlab.com/samuelbf/check_graphite/
"""

import json
import argparse
from copy import deepcopy
import urllib.request
import urllib.parse
import urllib.error
import sys

from enum import Enum
from numbers import Real

NAGIOS_STATUSES = {
    'OK': 0,
    'WARNING': 1,
    'CRITICAL': 2,
    'UNKNOWN': 3
}


class NagiosReturnCode(Enum):
    """Associates plugin status with script return code"""
    UNKNOWN = 3
    CRITICAL = 2
    WARNING = 1
    OK = 0


# pylint: disable=too-few-public-methods
class NagiosPerformanceData:
    """Hold Nagios compatible performance point

    Note: we don't support minimum/maximum, as we don't compute these values in check_graphite.

    Ref: https://nagios-plugins.org/doc/guidelines.html#AEN200
    """
    label = ''
    value = ''
    warn = ''
    crit = ''

    def __init__(self, label, value, warn='', crit=''):
        self.label = label.replace('=', '_').replace("'", '_')
        if ' ' in self.label:
            self.label = f"'{self.label}'"
        self.value = value
        self.warn = warn
        self.crit = crit

    def __str__(self):
        """Return nagios compliant form for performance data

        >>> print(NagiosPerformanceData('cpu_used', 0.2, 0.8, 0.95))
        cpu_used=0.2;0.8;0.95;;

        >>> print(NagiosPerformanceData('received metrics', 1234, 2000, 3000))
        'received metrics'=1234;2000;3000;;
        """
        return f"{self.label}={self.value};{self.warn};{self.crit};;"


class NagiosResult:
    """Hold Nagios compatible check results

    Ref: https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/4/en/pluginapi.html
    """
    return_code = NagiosReturnCode.UNKNOWN
    text_output = 'no information'
    perfdata = []
    long_output = ''
    long_perfdata = []

    # pylint: disable=too-many-arguments
    def __init__(self, return_code, text_output, perfdata=None, long_output='', long_perfdata=None):
        self.return_code = return_code
        self.text_output = text_output
        self.perfdata = perfdata if perfdata else []
        self.long_output = long_output
        self.long_perfdata = long_perfdata if long_perfdata else []

    def __str__(self):
        """Return nagios compliant string for plugin output

        >>> print(NagiosResult(NagiosReturnCode.UNKNOWN, 'no information'))
        GRAPHITE UNKNOWN : no information

        >>> print(NagiosResult(NagiosReturnCode.OK, 'CPU usage is 0.2', [NagiosPerformanceData('cpu_used', 0.2, 0.8, 0.95)]))
        GRAPHITE OK : CPU usage is 0.2 | cpu_used=0.2;0.8;0.95;;

        >>> print(NagiosResult(NagiosReturnCode.CRITICAL, 'CPU usage is 0.995 for vm-c',  \
                  perfdata = [NagiosPerformanceData('cpu_used by vm-c', 0.995, 0.8, 0.95)],   \
                  long_output = "WARNING : CPU usage is 0.9 for vm-a\\nOK : CPU usage is 0.02 for vm-b\\nCRITICAL : CPU usage is 0.995 for vm-c\\nOK : CPU usage is 0.3 for vm-d", \
                  long_perfdata = [                                                           \
                      NagiosPerformanceData('cpu_used by vm-a', 0.9, 0.8, 0.95),              \
                      NagiosPerformanceData('cpu_used by vm-b', 0.02, 0.8, 0.95),             \
                      NagiosPerformanceData('cpu_used by vm-d', 0.3, 0.8, 0.95)],))
        GRAPHITE CRITICAL : CPU usage is 0.995 for vm-c | 'cpu_used by vm-c'=0.995;0.8;0.95;;
        WARNING : CPU usage is 0.9 for vm-a
        OK : CPU usage is 0.02 for vm-b
        CRITICAL : CPU usage is 0.995 for vm-c
        OK : CPU usage is 0.3 for vm-d | 'cpu_used by vm-a'=0.9;0.8;0.95;; 'cpu_used by vm-b'=0.02;0.8;0.95;; 'cpu_used by vm-d'=0.3;0.8;0.95;;
        """
        plugin_output = f"GRAPHITE {self.return_code.name} : {self.text_output}"
        if self.perfdata:
            plugin_output += " | " + " ".join(map(lambda x: x.__str__(), self.perfdata))
        if self.long_output:
            plugin_output += f"\n{self.long_output}"
        if self.long_perfdata:
            plugin_output += " | " + " ".join(map(lambda x: x.__str__(), self.long_perfdata))

        return plugin_output

    def exit(self):
        """Print plugin result and exits with corresponding error code."""
        print(self)
        sys.exit(self.return_code.value)

    @staticmethod
    def from_nagios_results(nagios_results):
        """Merges several check results into one

        >>> print(NagiosResult.from_nagios_results([]))
        GRAPHITE UNKNOWN : no result given

        >>> print(NagiosResult.from_nagios_results([NagiosResult(NagiosReturnCode.OK, 'CPU usage is 0.4')]))
        GRAPHITE OK : CPU usage is 0.4

        >>> print(NagiosResult.from_nagios_results([                                                                                                     \
            NagiosResult(NagiosReturnCode.WARNING, 'CPU usage is 0.9 for vm-a', [NagiosPerformanceData('cpu_used by vm-a', 0.9, 0.8, 0.95)]),     \
            NagiosResult(NagiosReturnCode.OK, 'CPU usage is 0.2 for vm-b', [NagiosPerformanceData('cpu_used by vm-b', 0.02, 0.8, 0.95)]),        \
            NagiosResult(NagiosReturnCode.CRITICAL, 'CPU usage is 0.995 for vm-c', [NagiosPerformanceData('cpu_used by vm-c', 0.995, 0.8, 0.95)]), \
            NagiosResult(NagiosReturnCode.OK, 'CPU usage is 0.3 for vm-d', [NagiosPerformanceData('cpu_used by vm-d', 0.3, 0.8, 0.95)]),         \
            ]))
        GRAPHITE CRITICAL : CPU usage is 0.995 for vm-c | 'cpu_used by vm-c'=0.995;0.8;0.95;;
        WARNING : CPU usage is 0.9 for vm-a
        OK : CPU usage is 0.2 for vm-b
        OK : CPU usage is 0.3 for vm-d | 'cpu_used by vm-a'=0.9;0.8;0.95;; 'cpu_used by vm-b'=0.02;0.8;0.95;; 'cpu_used by vm-d'=0.3;0.8;0.95;;
        """
        if len(nagios_results) == 0:
            return NagiosResult(NagiosReturnCode.UNKNOWN, 'no result given')

        if len(nagios_results) == 1:
            return nagios_results[0]

        # Ordering by return code desc
        nagios_results.sort(reverse=True, key=lambda x: x.return_code.value)

        # Using first result as "main" result, adding other results in long_output / long_perfdata
        combined_result = deepcopy(nagios_results.pop(0))
        if combined_result.long_output:
            long_output_lines = [combined_result.long_output]
        else:
            long_output_lines = []
        long_perfdata = combined_result.long_perfdata

        while nagios_results:
            new_result = nagios_results.pop(0)
            long_output_lines += [f'{new_result.return_code.name} : {new_result.text_output}']
            if new_result.long_output:
                long_output_lines += [f'{new_result.long_output}']

            long_perfdata += (new_result.perfdata) + (new_result.long_perfdata)

        combined_result.long_output = '\n'.join(long_output_lines)
        combined_result.long_perfdata = long_perfdata

        return combined_result


def graphite_query_url(url, targets, _from, _until):
    """Computes graphite URL / query string for targets, from base url, targets, from/until parameters"""
    params = [('target', t) for t in targets] +\
        [('from', _from)] +\
        [('until', _until)] +\
        [('format', 'json')]
    return url.rstrip('/') + '/render?' + urllib.parse.urlencode(params)


def fetch_metrics(url, return_code_when_empty=NagiosReturnCode.UNKNOWN):
    """Fetch metrics from raphite server or exits with NagiosResult(return_code_when_empty).exit() on failure"""
    try:
        # pylint: disable=consider-using-with
        response = urllib.request.urlopen(url)

        if response.code != 200:
            return NagiosResult(return_code_when_empty, f'Server returned HTTP code {response.code}', long_output=f'URL: {url}\nResponse: {response.read()}').exit()

        return json.loads(response.read())
    except urllib.error.URLError as exception:
        return NagiosResult(return_code_when_empty, f'Request error: {exception.reason}', long_output=f'URL: {url}').exit()


def parse_args(args):
    """Parse arguments and display help for check_graphite"""
    parser = argparse.ArgumentParser(description='Check metrics from graphite API',
        formatter_class=argparse.RawTextHelpFormatter,
        epilog="""Examples:

- Check "metricsReceived <= 1200" in the last 10 minutes :
    $ check_graphite -U http://localhost:8888/ --from=-10minutes -t metricsReceived -W 1200
    GRAPHITE WARNING : metricsReceived is 1403.0 (highest value) | metricsReceived=1403.0;1200.0;;;
    metricsReceived=None/None/1403.0/1387.0/615.0/618.0/615.0/621.0/None/None

- Check "metricsReceived <= 1200" yesterday for at least 70% of values :
    $ check_graphite -U http://localhost:8888/ -t metricsReceived -W 1200 -C 1400 --percentile=70 --from=yesterday --until=today
    GRAPHITE WARNING : metricsReceived is 1387.0 (70th percentile) | 'nPercentile(metricsReceived, 70)'=1387.0;1200.0;1400.0;;

- Check "metricsReceived <= 1200" and "committedPoints <= 1200" for at least 70% of values in the last 10 minutes :
    $ check_graphite -U http://localhost:8888/ --from=-10minutes -t metricsReceived -t committedPoints -W 1200 -C 1400 --percentile=70
    GRAPHITE WARNING : metricsReceived is 1387.0 (70th percentile) | 'nPercentile(metricsReceived, 70)'=1387.0;1200.0;1400.0;;
    OK : committedPoints is 639.0 (70th percentile) | 'nPercentile(committedPoints, 70)'=639.0;1200.0;1400.0;;

- Check "metricsReceived" and "committedPoints" not over 1200 more than 2 times in the last 10 minutes :
    $ check_graphite -U http://localhost:8888/ --from=-10minutes -t "aliasByMetric(carbon.agents.*.{metricsReceived,committedPoints})" -W 1200 -C 1400 --count=3
    GRAPHITE OK : committedPoints is 636.0 (third highest value) | committedPoints_3=636.0;1200.0;1400.0;;
    committedPoints=None/None/0.0/2692.0/599.0/636.0/633.0/639.0/None/None
    OK : metricsReceived committedPoints 621.0 (third highest value)
    metricsReceived=None/None/1403.0/1387.0/615.0/618.0/615.0/621.0/None/None | metricsReceived_3=621.0;1200.0;1400.0;;
    """)
    parser.add_argument('-U', '--graphite-url', dest='graphite_url',
                        default='http://localhost/',
                        metavar='URL',
                        help='Graphite URL [%(default)s]')
    parser.add_argument('-t', '--target', dest='targets', required=True,
                        action='append',
                        help='Target to check')
    parser.add_argument('--from', dest='graphite_from', required=True,
                        help='From timestamp/date')
    parser.add_argument('--until', dest='graphite_until',
                        default='now',
                        help='Until timestamp/date [%(default)s]')
    parser.add_argument('-W', '--warning', dest='warning',
                        type=float,
                        metavar='WARN',
                        help='Warning if datapoints over WARNING')
    parser.add_argument('-C', '--critical', dest='critical',
                        type=float,
                        metavar='CRIT',
                        help='Critical if datapoints over CRITICAL')
    percentile_or_percent = parser.add_mutually_exclusive_group()
    percentile_or_percent.add_argument('-c', '--count', dest='count',
                                       default=1,
                                       type=int,
                                       help='Alert when at least COUNT metrics are over/under thresholds [%(default)s]')
    percentile_or_percent.add_argument('--percentile', dest='percentile',
                                       default=None,
                                       type=int,
                                       metavar='PERCENT',
                                       help='Use nPercentile Graphite function on the target (returns one datapoint)')
    parser.add_argument('--over', dest='over',
                        default=True,
                        action='store_true',
                        help='Alert when data OVER specified WARNING or CRITICAL threshold [%(default)s]')
    parser.add_argument('--under', dest='over',
                        default=False,
                        action='store_false',
                        help='Alert when data UNDER specified WARNING or CRITICAL threshold [%(default)s]')
    parser.add_argument('--empty-ok', dest='return_code_when_empty',
                        default=NagiosReturnCode.UNKNOWN,
                        action='store_const',
                        const=NagiosReturnCode.OK,
                        help='Empty data from Graphite is OK')

    # Rewriting "--from xxx" as "--from=xxx", as "-10minutes" is a legitimate value
    # Ref: https://docs.python.org/3/library/argparse.html#arguments-containing
    if "--from" in args:
        index = args.index("--from")
        if len(args) > index + 1:
            args[index] += f"={args[index+1]}"
            args.pop(index + 1)

    return parser.parse_args(args)


# pylint: disable=too-many-arguments, too-many-locals, too-many-branches
def process_static_threshold(series, over=True, return_code_when_empty=NagiosReturnCode.UNKNOWN,
                             count=1, critical=None, warning=None, percentile=None):
    """Compute NagiosResult for a single series

    >>> series = {'target':'metricsReceived', 'datapoints':[[None],[None],[1403.0],[1387.0],[615.0],[618.0],[615.0],[621.0],[None],[None]]}
    >>> print(process_static_threshold(series, warning=1200, critical=2000))
    GRAPHITE WARNING : metricsReceived is 1403.0 (highest value) | metricsReceived=1403.0;1200;2000;;
    metricsReceived=None/None/1403.0/1387.0/615.0/618.0/615.0/621.0/None/None

    >>> print(process_static_threshold(series, warning=1000, critical=2000, count=3))
    GRAPHITE OK : metricsReceived is 621.0 (third highest value) | metricsReceived_3=621.0;1000;2000;;
    metricsReceived=None/None/1403.0/1387.0/615.0/618.0/615.0/621.0/None/None

    >>> print(process_static_threshold({'target':'metricsReceived', 'datapoints':[[None],[None],[None]]}, warning=1000))
    GRAPHITE UNKNOWN : metricsReceived exists but has no valid point in interval.
    metricsReceived=None/None/None

    >>> print(process_static_threshold(series, warning=1000, count=7))
    GRAPHITE UNKNOWN : Only 6 points available for metricsReceived (< 7)
    metricsReceived=None/None/1403.0/1387.0/615.0/618.0/615.0/621.0/None/None

    >>> series_percentile = {'target':'nPercentile(metricsReceived, 70)', 'datapoints': [[1387.0], [1387.0], [1387.0]]}
    >>> print(process_static_threshold(series_percentile, warning=1000, percentile=70))
    GRAPHITE WARNING : metricsReceived is 1387.0 (70th percentile) | 'nPercentile(metricsReceived, 70)'=1387.0;1000;;;
    """
    datapoints = series.get('datapoints')
    target_name = series.get("target")
    if percentile is None:
        if len(datapoints) < 30:
            long_output = f'{target_name}=' + '/'.join(map(lambda x: str(x[0]), datapoints))
        else:
            long_output = f'{target_name}=' + '/'.join(map(lambda x: str(x[0]), datapoints[:15])) + '/.../' + '/'.join(map(lambda x: str(x[0]), datapoints[-10:]))
    else:
        long_output = ''

    datapoints = list(map(lambda x: x[0], filter(lambda x: isinstance(x[0], Real), datapoints)))

    if len(datapoints) < count:
        if len(datapoints) == 0:
            return NagiosResult(return_code_when_empty, f'{target_name} exists but has no valid point in interval.', [], long_output)

        return NagiosResult(return_code_when_empty, f'Only {len(datapoints)} points available for {target_name} (< {count})', [], long_output)

    datapoints.sort(reverse=over)
    # pylint: disable=invalid-name
    if over:
        def compare_func(x, y): return x > y
    else:
        def compare_func(x, y): return x < y

    if percentile is not None:
        retained_value = datapoints[0]
        perflabel = target_name
        legend_target_name = target_name[12:-(len(str(percentile)) + 3)]
        legend_rank = {1: 'first ', 2: 'second', 3: 'third'}.get(percentile, f'{percentile}th') + ' percentile'
    else:
        retained_value = datapoints[count - 1]
        perflabel = target_name if count == 1 else f"{target_name}_{count}"
        legend_target_name = target_name
        legend_rank = {1: '', 2: 'second ', 3: 'third '}.get(count, f'{count}th ') + ('highest value' if over else 'lowest value')

    legend = f"{legend_target_name} is {retained_value} ({legend_rank})"

    perfdata = [NagiosPerformanceData(perflabel, retained_value, warning if warning is not None else '', critical if critical is not None else '')]

    if critical is not None and compare_func(retained_value, critical):
        return_code = NagiosReturnCode.CRITICAL
    elif warning is not None and compare_func(retained_value, warning):
        return_code = NagiosReturnCode.WARNING
    else:
        return_code = NagiosReturnCode.OK

    return NagiosResult(return_code, legend, perfdata, long_output)


def main(argv):
    """Process command line options from argv, fetch metric, compute and output result, then exit with corresponding return code"""
    options = parse_args(argv[1:])

    real_targets = options.targets
    if options.percentile:
        real_targets = map(lambda name: f"nPercentile({name}, {options.percentile})", real_targets)

    metric_data = fetch_metrics(graphite_query_url(options.graphite_url, real_targets, options.graphite_from, options.graphite_until), options.return_code_when_empty)
    if len(metric_data) == 0:
        NagiosResult(options.return_code_when_empty, f'Server returned no data for series {", ".join(real_targets)}').exit()

    def process_series(series):
        return process_static_threshold(series, options.over, options.return_code_when_empty, options.count,
                                        options.critical, options.warning, options.percentile)

    nagios_results = list(map(process_series, metric_data))

    NagiosResult.from_nagios_results(nagios_results).exit()


if __name__ == '__main__':
    main(sys.argv)
